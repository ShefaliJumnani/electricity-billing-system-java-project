/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electricity.billing.system;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Splash {
    public static void main(String[] args){
        //create the object of fframe class
        fframe f1=new fframe();  //why did it show the error of can not be referenced from a non static context earlier?
        f1.setVisible(true); //to see the frame
        
        //take two variables for location and size since both change continuously for the image
        int i;
        int x=1;
        for(i=2;i<=600;i+=4,x+=1){ //i is iterated till the length we req for the image
            f1.setLocation(800- ((i+x)/2), 500-(i/2)); //what does this do??
            f1.setSize(i+x,i);
            try{
                Thread.sleep(10);
            }catch(Exception e){}
    }
}
}

class fframe extends JFrame implements Runnable{
    Thread t1;
    fframe(){
        super("Electricity Billing System");
        setLayout(new FlowLayout()); //to use flow layout,we create flow layout object.
        //coding for image:
        ImageIcon c1=new ImageIcon(ClassLoader.getSystemResource("icon/elect.jpg"));
        Image i1=c1.getImage().getScaledInstance(730, 550, Image.SCALE_DEFAULT);
        ImageIcon i2=new ImageIcon(i1);
        
        JLabel l1=new JLabel(i2); //create a new label to add the image tp the frame
        add(l1); //add the label to the frame
        t1=new Thread(this); //create a new thread class object-->we req multithreading
        t1.start(); //start the object of thread class
    }
    
    //override the run method
    public void run(){
        try{
            Thread.sleep(7000); //arg is passed in ms
            this.setVisible(false);
            //closes the current frame
            
            Login l = new Login();
            l.setVisible(true);
            }catch (Exception e){
                e.printStackTrace();
            }
    }
}