
package electricity.billing.system;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;

public class Customer_Details extends JFrame implements ActionListener {
    
    JTable t1;
    JButton b1;
    
    //heading of rows:
    String x[]={"Emp Name","Meter No","Address","State","City","Email","Phone"};
    
    //2-d array to store data
    String[][] y=new String[20][8];
    int i=0,j=0;
    Customer_Details(){
        super("Customer Details");
        setSize(1200,650);
        setLocation(200,200);
        
        try{
            conn c1=new conn();
            String s1="select * from emp"; //to retireive entire data
            ResultSet rs=c1.s.executeQuery(s1);
            while(rs.next()){
                //store value in each row:
                y[i][j++]=rs.getString("name");
                y[i][j++]=rs.getString("meter_number");
                y[i][j++]=rs.getString("address");
                y[i][j++]=rs.getString("state");
                y[i][j++]=rs.getString("city");
                y[i][j++]=rs.getString("email");
                y[i][j++]=rs.getString("phone");
                i++;
                j=0;
            }
            t1=new JTable(y,x);
            }catch(Exception ex){
                ex.printStackTrace();
        }
        b1=new JButton("Print");
        add(b1,"South");
        JScrollPane sp=new JScrollPane(t1); //add scroll pane to table t1
        add(sp);
        b1.addActionListener(this);
        
    }
    public void actionPerformed(ActionEvent ae){
        try{
            t1.print();
        }catch(Exception e){}
    }
    
    public static void main(String[] args){
        new Customer_Details().setVisible(true);
    }
    
}
      