
package electricity.billing.system;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;


public class Login extends JFrame implements ActionListener{
    //Declare global variables so that they are accessible everywhere in the class
    JLabel l1,l2,l3;
    JTextField tf1;
    JPasswordField pf2;
    JButton b1,b2;
    JPanel p1,p2,p3,p4;
    Login(){ //constructor
        super("Login Page");
        
        l1=new JLabel("Username");
        l2=new JLabel("Password");
        tf1=new JTextField(15); //creates a text field of 15 columns
        pf2=new JPasswordField(15);
        
        ImageIcon ic1=new ImageIcon(ClassLoader.getSystemResource("icon/login.png"));
        // TO RESIZE THE IMAGE AS PER REQUIREMENT
        Image i1=ic1.getImage().getScaledInstance(16, 16, Image.SCALE_DEFAULT);
        b1=new JButton("Login",new ImageIcon(i1)); //adds image in the button itself
        
        ImageIcon ic2=new ImageIcon(ClassLoader.getSystemResource("icon/cancel.jpg"));
        Image i2=ic2.getImage().getScaledInstance(16, 16, Image.SCALE_DEFAULT);
        b2=new JButton("Cancel",new ImageIcon(i2));
        
        b1.addActionListener(this); //defines the action to be performed on this button(login)
        b2.addActionListener(this);
        
        ImageIcon ic3=new ImageIcon(ClassLoader.getSystemResource("icon/pop.png"));
        Image i3=ic3.getImage().getScaledInstance(128, 128, Image.SCALE_DEFAULT);
        ImageIcon iccc3=new ImageIcon(i3);
        l3=new JLabel(iccc3);
        
        setLayout(new BorderLayout());
        
        p1=new JPanel();
        p2=new JPanel();
        p3=new JPanel();
        p4=new JPanel();
        
        add(l3,BorderLayout.WEST);
        p2.add(l1);   //add l1,tf1,l2,pf2 in panel p2
        p2.add(tf1);
        p2.add(l2);
        p2.add(pf2);
        
        add(p2,BorderLayout.CENTER);
        
        p4.add(b1);
        p4.add(b2);
        add(p4,BorderLayout.SOUTH);
        
        p2.setBackground(Color.WHITE);
        p4.setBackground(Color.WHITE);
        
        
        setSize(440,250); //sets size of the frame 440-->length 250-->breadth
        setLocation(600,400); //x=600 y=400 frame will always open at this location by default
        setVisible(true);  //frames are set to false in setvisible by default.We need to set them to true in order to make them visible.
        
        
        }
    
    //actionPerfromed is the func that should be overridden and defines the action to be performed.
    //we also create the object,here,ae,of actionEvenet class
    public void actionPerformed(ActionEvent ae){
        
        try{
            conn c1=new conn();  //create the object of ceated connection class.
            String a=tf1.getText(); //getText() retrieves the values entered in the field.
            String b=pf2.getText(); //retrieves the value entred in tf1 and pf2 and stores it in a and b.
            String q="select * from login where username = '"+a+"' and password='"+b+"'";  //here a and b refer to the above entered strings.store the entire query in q.
            ResultSet rs=c1.s.executeQuery(q);  //c1 is the conn ibject.s is connection url(that establises connection with database). executequery is used to retrieve the values whereas execute update is used to update the values.
            
            //result set goes through each and every column in the table and matches and retrieves the values from the table
            
            if(rs.next()){ //if user name and password match
                
              //it opens a new project class
              
                new Project().setVisible(true); //create the object of project class and set it to visible so as to open the frame of that class.
                this.setVisible(false); //now since when there is a match,the new frame of project frame opens.We need to close this (login)frame. 
                
            }
            else{
                //if values do not match:
                
                //display a dialog box showing invalid credentials
                
                JOptionPane.showMessageDialog(null,"Invalid login");
                //JOptionPane is used to display the pop-up.
             
                setVisible(false);
                }
        }catch(Exception e){
                    e.printStackTrace();
                    System.out.println("error: "+e);
                    }
        }
        public static void main(String[] args){
            new Login().setVisible(true);
        }
    }

